import unittest
import lab2


class TestTasks(unittest.TestCase):
    def testIsOddAndDoch(self):
        self.assertTrue(lab2.is_odd_and_doch(22))
        self.assertTrue(lab2.is_odd_and_doch(10))
        self.assertTrue(lab2.is_odd_and_doch(98))
        self.assertTrue(lab2.is_odd_and_doch(50))

        self.assertFalse(lab2.is_odd_and_doch(21))
        self.assertFalse(lab2.is_odd_and_doch(1))
        self.assertFalse(lab2.is_odd_and_doch(0))
        self.assertFalse(lab2.is_odd_and_doch(100))

    def test_is_divide(self):
        self.assertTrue(lab2.is_divide(2, 2))
        self.assertTrue(lab2.is_divide(20, 10))
        self.assertTrue(lab2.is_divide(100, 4))
        self.assertTrue(lab2.is_divide(9, 3))
        self.assertTrue(lab2.is_divide(24, 8))
        self.assertTrue(lab2.is_divide(24, 3))

        self.assertFalse(lab2.is_divide(13, 2))
        self.assertFalse(lab2.is_divide(27, 8))
        self.assertFalse(lab2.is_divide(18, 10))
        self.assertFalse(lab2.is_divide(14, 6))

    def test_get_more_than_ten(self):
        self.assertEqual(lab2.get_more_than_ten([1, 2, 5, 10, 11, 12]), 11)
        self.assertEqual(lab2.get_more_than_ten([12, 11, 10, 9, 8, 7]), 12)
        self.assertEqual(lab2.get_more_than_ten([10]), "Нет таких")


    def test_prime_numbers(self):
        primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
                  73, 79, 83, 89, 97]
        self.assertEqual(lab2.prime_numbers(10), [2, 3, 5, 7])
        self.assertEqual(lab2.prime_numbers(100), primes)


    def test_squaring_key(self):
        dct1 = {1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36, 7: 49}
        self.assertDictEqual(lab2.squaring_key(list(range(1, 8))), dct1)


    def test_dict_sum(self):
        dct1 = {1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36, 7: 49}
        self.assertEqual(lab2.dict_sum(dct1), 168)


    def test_f(self):
        self.assertEqual(lab2.f(12), 14)
        self.assertEqual(lab2.f(1), -8)
        self.assertEqual(lab2.f(0), 0)
        self.assertEqual(lab2.f(-1), 1)
        self.assertEqual(lab2.f(-10), 19)


    def test_syracuse_hypothesis(self):
        self.assertEqual(lab2.syracuse_hypothesis(20), 1)
        self.assertEqual(lab2.syracuse_hypothesis(21), 1)
        self.assertEqual(lab2.syracuse_hypothesis(22), 1)
        self.assertEqual(lab2.syracuse_hypothesis(23), 1)
        self.assertEqual(lab2.syracuse_hypothesis(24), 1)
        self.assertEqual(lab2.syracuse_hypothesis(25), 1)
        self.assertEqual(lab2.syracuse_hypothesis(26), 1)
        self.assertEqual(lab2.syracuse_hypothesis(27), 1)
        self.assertEqual(lab2.syracuse_hypothesis(28), 1)
        self.assertEqual(lab2.syracuse_hypothesis(29), 1)
        self.assertEqual(lab2.syracuse_hypothesis(30), 1)
        self.assertEqual(lab2.syracuse_hypothesis(223), 1)
        self.assertEqual(lab2.syracuse_hypothesis(2223), 1)


    def test_star_w(self):
        w = [['*', ' ', ' ', ' ', ' ', ' ', '*', ' ', ' ', ' ', ' ', ' ', '*'],
             [' ', '*', ' ', ' ', ' ', '*', ' ', '*', ' ', ' ', ' ', '*', ' '],
             [' ', ' ', '*', ' ', '*', ' ', ' ', ' ', '*', ' ', '*', ' ', ' '],
             [' ', ' ', ' ', '*', ' ', ' ', ' ', ' ', ' ', '*', ' ', ' ', ' ']]
        self.assertListEqual(w, lab2.star_w())


    def test_get_k_digit(self):
        self.assertEqual(lab2.get_k_digit(1), 1)
        self.assertEqual(lab2.get_k_digit(2), 0)
        self.assertEqual(lab2.get_k_digit(3), 1)
        self.assertEqual(lab2.get_k_digit(4), 1)
        self.assertEqual(lab2.get_k_digit(5), 1)
        self.assertEqual(lab2.get_k_digit(6), 2)
        self.assertEqual(lab2.get_k_digit(7), 1)
        self.assertEqual(lab2.get_k_digit(8), 3)
        self.assertEqual(lab2.get_k_digit(9), 1)
        self.assertEqual(lab2.get_k_digit(10), 4)
        self.assertEqual(lab2.get_k_digit(11), 1)
        self.assertEqual(lab2.get_k_digit(12), 5)
        self.assertEqual(lab2.get_k_digit(13), 1)
        self.assertEqual(lab2.get_k_digit(14), 6)
        self.assertEqual(lab2.get_k_digit(15), 1)
        self.assertEqual(lab2.get_k_digit(16), 7)
        self.assertEqual(lab2.get_k_digit(17), 1)
        self.assertEqual(lab2.get_k_digit(18), 8)
        self.assertEqual(lab2.get_k_digit(19), 1)
        self.assertEqual(lab2.get_k_digit(20), 9)
        

if __name__ == '__main__':
    unittest.main()
