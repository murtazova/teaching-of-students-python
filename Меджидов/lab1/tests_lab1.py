import unittest
import lab1


class Testlab1(unittest.TestCase):
    def setUp(self):
        self.source_lst = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    def test_sort_by_odd_and_even(self):
        result_lst = [1, 3, 5, 7, 9, 2, 4, 6, 8, 10]
        self.assertEqual(lab1.sort_by_odd_and_even(
            self.source_lst), result_lst)

        self.assertEqual(lab1.sort_by_odd_and_even([]), [])
        self.assertEqual(lab1.sort_by_odd_and_even([1]), [1])
        self.assertEqual(lab1.sort_by_odd_and_even([1, 2]), [1, 2])
        self.assertEqual(lab1.sort_by_odd_and_even([2, 1]), [1, 2])
        self.assertEqual(lab1.sort_by_odd_and_even([2, 2]), [2, 2])

    def test_replace_first_and_last(self):
        source_lst = [1, 2, 3, 4, 5]
        result_lst = [5, 2, 3, 4, 1]
        self.assertEqual(lab1.replace_first_and_last(source_lst), result_lst)

    def test_sum_of_mod(self):
        source_lst1 = [1, 2, 3, 4, 5]
        source_lst2 = [5, 4, 3, 2, 1]
        self.assertEqual(lab1.sum_of_mod(source_lst1), 0)
        self.assertEqual(lab1.sum_of_mod(source_lst2), 3)

    def test_sum_function(self):
        """Проверяем все функции суммирования"""
        self.assertEqual(lab1.for_sum(self.source_lst), 55)
        self.assertEqual(lab1.while_sum(self.source_lst), 55)
        self.assertEqual(lab1.recursion_sum(self.source_lst), 55)
        self.assertEqual(lab1.sum_sum(self.source_lst), 55)
        self.assertEqual(lab1.reduce_sum(self.source_lst), 55)

    def test_my_map(self):
        self.assertEqual(lab1.my_map([1, 2, 3], [11, 22, 33]), [
                         1, 11, 2, 22, 3, 33])

    def test_fibonacci(self):
        self.assertEqual(lab1.fibonacci(1), 1)
        self.assertEqual(lab1.fibonacci(2), 1)
        self.assertEqual(lab1.fibonacci(3), 2)
        self.assertEqual(lab1.fibonacci(4), 3)
        self.assertEqual(lab1.fibonacci(5), 5)
        self.assertEqual(lab1.fibonacci(6), 8)
        self.assertEqual(lab1.fibonacci(100), 354224848179261915075)

    def test_first_digit(self):
        self.assertEqual(lab1.first_digit(1), 1)
        self.assertEqual(lab1.first_digit(12), 1)
        self.assertEqual(lab1.first_digit(21), 2)
        self.assertEqual(lab1.first_digit(333), 3)
        self.assertEqual(lab1.first_digit(9333), 9)

    def test_list_to_number(self):
        self.assertEqual(lab1.list_to_number([61, 228, 9]), 961228)
        self.assertEqual(lab1.list_to_number([228, 61, 9]), 961228)
        self.assertEqual(lab1.list_to_number([678, 98, 9]), 998678)
        self.assertEqual(lab1.list_to_number([61, 9, 98]), 99861)
        self.assertEqual(lab1.list_to_number([9, 61, 98]), 99861)


if __name__ == '__main__':
    unittest.main()
