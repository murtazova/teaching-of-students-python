from math import pi

# Создать класс с двумя переменными.
# Добавить конструктор с входными параметрами и деструктор,
# выводящий на экран сообщение об удалении объекта.
# Добавить функцию вывода на экран и функцию изменения этих переменных.
# Добавить функцию, которая находит сумму значений этих переменных,
# и функцию которая находит наибольшее значение из этих двух переменных.

class TwoVar:
    def __init__(self, var1=0, var2=0):
        self.var1 = var1
        self.var2 = var2

    def __del__(self):
        print(f"Мы {self.var1} {self.var2} вынуждены покинуть тебя. Прощай!\n")

    def show(self):
        print(f"{self.var1} {self.var2}")

    def update(self, var1, var2):
        self.var1 = var1
        self.var2 = var2

    def sum(self):
        return self.var1 + self.var2

    def max(self):
        return max(self.var1, self.var2)


# Создать класс Figure с методами вычисления площади и периметра,
# а также методом, выводящим информацию о фигуре на экран.
# Создать производные классы:
        # Rectangle (прямоугольник),
        # Circle (круг),
        # Triangle (треугольник)
# со своими методами вычисления площади и периметра.
# Создать массив n фигур и вывести полную информацию о фигурах на экран.

class Figure:
    """Общий класс Фигура"""

    def square(self):
        """Абстрактный метод вычисления площади"""
        raise NotImplementedError("Необходимо переопределить метод")

    def parameter(self):
        """Абстрактный метод вычисления периметра"""
        raise NotImplementedError("Необходимо переопределить метод")


class Rectangle(Figure):
    def __init__(self, a, b):
        super().__init__()
        self.a = a
        self.b = b

    def square(self):
        return self.a * self.b

    def parameter(self):
        return self.a + self.a + self.b + self.b


class Triangle(Figure):
    def __init__(self, a, b, c):
        super().__init__()
        self.a = a
        self.b = b
        self.c = c

    def square(self):
        if not self.is_exist():
            return 0

        p = self.parameter() / 2
        return (p * (p - self.a) * (p - self.b) * (p - self.c))**0.5

    def parameter(self):
        if not self.is_exist():
            return 0
        return self.a + self.b + self.c

    def is_exist(self):
        return (self.a + self.b > self.c
            and self.a + self.c > self.b
            and self.b + self.c > self.a)
    

class Circle(Figure):
    def __init__(self, r):
        super().__init__()
        self.r = r
        
    def square(self):
        return self.r * self.r * pi

    def parameter(self):
        return 2 * self.r * pi


# Создайте структуру с именем Student,
# содержащую поля:
# фамилия,
# номер группы
# и успеваемость
# в виде массив из пяти элементов
# (оценки студента можно смоделировать рандомно).
# Создать массив из десяти элементов такого типа,
# упорядочить записи по возрастанию среднего балла.
# Добавить возможность вывода фамилий
# и номеров групп студентов, имеющих оценки, равные только 4 или 5.
class Student:
    def __init__(self, last_name, group_number, marks):
        self.last_name = last_name
        self.group_number = group_number
        self.marks = marks

    def average_mark(self):
        return round(sum(self.marks) / len(self.marks), 2)

    
    def is_good_student(self):
        good_mark = 4
        return all(map(lambda x: x >= good_mark, self.marks))



# Задача на взаимодействие между классами.
# Разработайте класс Число,
# в конструктор которого передается целочисленное значение,
# и в котором перегружены следующие операции:
# сложение, вычитание, умножение, целочисленное деление,
# остаток от деления и сравнение.
# Например, можно сложить два Числа с помощью ‘+’
# и получить третье Число, значение которого является
# суммой значений классов-слагаемых
class Number:
    def __init__(self, a):
        self.a = int(a)

    def __str__(self):
        return str(self.a)
    
    def __add__(self, other):
        return Number(self.a + other.a)

    def __sub__(self, other):
        return Number(self.a - other.a)

    def __mul__(self, other):
        return Number(self.a * other.a)

    def __truediv__(self, other):
        return Number(self.a / other.a)

    def __floordiv__(self, other):
        return self.__truediv__(other)

    def __mod__(self, other):
        return Number(self.a % other.a)

    def __eq__(self, other):
        return self.a == other.a

    def __ne__(self, other):
        return self.a != other.a

    def __lt__(self, other):
        return self.a < other.a

    def __gt__(self, other):
        return self.a > other.a

    def __le__(self, other):
        return self.a <= other.a

    def __ge__(self, other):
        return self.a >= other.a

    def __pow__(self, other):
        return Number(self.a ** other.a)

    def __pos__(self):
        return Number(+self.a) 

    def __neg__(self):
        return Number(-self.a) 


a = Number(2)
b = Number(1)

print(+a)

